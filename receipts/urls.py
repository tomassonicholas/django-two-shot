from django.urls import path
from receipts.views import (
    receipt_list_view,
    create_receipt,
    account_list,
    category_list,
    create_account,
    create_category,
)


urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
]
