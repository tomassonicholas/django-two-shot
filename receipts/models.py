from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class ExpenseCategory(models.Model):
    """Model definition for ExpenseCategory."""

    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    class Meta:
        """Meta definition for ExpenseCategory."""

        verbose_name = "ExpenseCategory"
        verbose_name_plural = "ExpenseCategorys"

    def __str__(self):
        return self.name


class Account(models.Model):
    """Model definition for Account."""

    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    class Meta:
        """Meta definition for Account."""

        verbose_name = "Account"
        verbose_name_plural = "Accounts"

    def __str__(self):
        return self.name


class Receipt(models.Model):
    """Model definition for Receipt."""

    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    class Meta:
        """Meta definition for Receipt."""

        verbose_name = "Receipt"
        verbose_name_plural = "Receipts"

    def __str__(self):
        return f"{self.vendor} on {self.date}"
