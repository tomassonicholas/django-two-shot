from django.shortcuts import (
    render,
    redirect,
)
from receipts.models import (
    Account,
    ExpenseCategory,
    Receipt,
)
from receipts.forms import (
    ReceiptForm,
    ExpenseCategoryForm,
    AccountForm,
)
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required()
def receipt_list_view(request):
    reciept_list = Receipt.objects.filter(purchaser=request.user).all()
    context = {
        "receipt_list": reciept_list,
    }
    return render(request, "receipts/receipt_view.html", context)


@login_required()
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.instance.purchaser = request.user
            form.save()
            return redirect("/receipts/")
    else:
        form = ReceiptForm
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    # receipts = Receipt.objects.filter(category=category_list.first().name)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/category_view.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    # receipts = Receipt.objects.filter(account=account_list.first().name)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/account_view.html", context)


@login_required()
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required()
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
